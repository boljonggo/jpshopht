import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/table/list',
    method: 'get',
    params
  })
}

export function getTest(parames) {
  return request({
    url: '/table/test',
    method: 'get',
    parames
  })
}
