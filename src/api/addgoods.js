import request from '@/utils/request'
/**
* 商品添加页面相关API
*/
/**
 * 商品分组
 * @param {*} params 
 */
export function getGoodsGroupList(params) {
  return request({
    url: '/merchantCategoryTypeMini',
    method: 'get',
    params
  })
}
/**
 * 获取商品销售区域
 * @param {*} params 
 */
export function getGoodsArea(params) {
  return request({
    url: '/merchantGoodsCityGroup',
    method: 'get',
    params
  })
}
/**
 * 上传商品视频Url
 */
export function postGoodsVideoUrl() {
  return process.env.VUE_APP_BASE_API+'/merchantGoodsVideo';
}
/**
 * 添加商品
 * @param params
 */
export function postGoods(params) {
  return request({
    url: '/merchantGoods',
    method: 'post',
    data: params
  })
}
/**
 * 添加商品到好物圈
 * @param params
 */
export function shopCircleGoods(params) {
  return request({
    url: '/shopCircleGoods',
    method: 'post',
    data: params
  })
}
